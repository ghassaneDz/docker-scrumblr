#! /bin/bash

test_container=$(curl -LI http://localhost -o /dev/null -w '%{http_code}\n' -s)

if [ "$test_container" = "200" ]
then
    echo "Container OK"
    exit 0
else
    echo "Container KO"
    exit 1
fi
